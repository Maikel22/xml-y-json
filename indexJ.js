    

document.getElementById('boton').addEventListener('click', obtenerdatos);
function obtenerdatos() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'index.json', true);
    xhr.send();
    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let datos = JSON.parse(this.responseText);
            console.log(datos);
            let res = document.querySelector('#res');
            res.innerHTML = '';
            for (let item of datos) {
                res.innerHTML += `<tr>
                <td>${item.apellidos}</td>
                <td>${item.nombres}</td>
                <td>${item.semestre}</td>
                <td>${item.paralelo}</td>
                <td>${item.direccion}</td>
                <td>${item.telefono}</td>
                <td>${item.correo}</td>
                </tr>`;
            }
            var table= document.getElementById("buscar");
            
            table.style.display="block"
            
        }
    };
}